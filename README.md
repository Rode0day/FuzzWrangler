# Rode0day FuzzWrangler

A distributed fuzzing management framework, a rewrite of LuckyCAT

- Use a single database
  - copy `single_db_example.env` to `.env`
  - copy `single_db-docker-compose.yaml` to `docker-compose.yaml`
  - modify secrets, database names as required

- Use multiple-databases
  - copy `example.env` to `.env`
  - clone the repo `git clone https://github.com/mrts/docker-postgresql-multiple-databases`
  - modify secrets, database names as required


- build / start the services `docker-compose up -d --build`
