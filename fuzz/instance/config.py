# /instance/config.py

import os
from urllib.parse import quote_plus as urlquote


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    CSRF_ENABLED = True
    SECRET = os.getenv('SECRET')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_ENGINE_OPTIONS = {'pool_size': 10, 'max_overflow': 20}
    CELERY_BROKER_URL = 'amqp://',
    CELERY_RESULT_BACKEND = 'rpc://'


class DevelopmentConfig(Config):
    """Configurations for Development."""
    SQLALCHEMY_DATABASE_URI = 'sqlite:///fuzz.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CSRF_ENABLED = False
    DEBUG = True


class TestingConfig(Config):
    """Configurations for Testing, with a separate test database."""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql:///fuzz_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True


class StagingConfig(Config):
    """Configurations for Staging."""
    DB_PASSWORD = os.getenv('DB_PASSWORD', 'LuckyFuzz')
    DB_URI = os.getenv('DB_URI', 'postgresql://postgres:%s@database:5432/fuzz_db')
    SQLALCHEMY_DATABASE_URI = DB_URI % urlquote(DB_PASSWORD)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = False
    TESTING = False
    CELERY_BROKER_URL = 'amqp://queue:5672',
    DEBUG = False


class ProductionConfig(Config):
    """Configurations for Production."""
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
}
