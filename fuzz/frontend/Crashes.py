import base64
import io
import json
import logging
import os
from difflib import SequenceMatcher

from flask import render_template, redirect, send_file, flash, request, Blueprint
from flask_security import login_required, current_user

from fuzz.database import db
from fuzz.database.models import Crash, Job, Target
from fuzz.frontend.api.CrashesApi import local_verify_crash

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

crashes = Blueprint('crashes', __name__)


def _get_job_name_from_id(job_id):
    job = Job.objects.get(id=job_id)
    return job.name


def _get_job_id_of_job_name(job_name):
    for job in Job.objects:
        if job_name == job.name:
            return job.id


def _get_job_ids_of_user():
    job_ids = []
    for job in Job.objects:
        if job.owner and job.owner.email == current_user.email:
            job_ids.append(job.id)
    return job_ids


def _get_job_names_of_user():
    job_ids = _get_job_ids_of_user()
    job_names = []
    for job_id in job_ids:
        job_names.append(get_job_name_of_job_id(job_id))
    return job_names


def delete_crash(crash_id):
    crash = Crash.query.get(crash_id)
    if crash and current_user.has_role('admin'):
        try:
            db.session.delete(crash)
            db.session.commit()
            return True
        except Exception as e:
            logger.error("delete_crash error: %s", e)
    return False


@crashes.route('/crashes/show')
def show_crashes(crashes=None):
    # FIXME show colors again
    # FIXME fix checkboxes and so on!!!
    # FIXME just show first ten results, make it expandable
    if crashes:
        # arriving from search_crash
        crashes = crashes

    # job_ids = _get_job_ids_of_user()
    # final_res = collections.defaultdict(list)
#    for j in Job.query.all():
#        final_res[j.name] = j.crashes
#        logger.error(len(j.crashes))
    cols = [Target.id, Target.name, Target.filename, Target.bin_hash]
    target_list = db.session.query(*cols).all()
    if request.values.get('job_id'):
        results_list = [Job.query.get(request.values.get('job_id'))]
    elif request.values.get('target_id'):
        results_list = Job.query.filter_by(target_id=request.values.get('target_id')).all()
    else:
        results_list = Job.query.filter_by(target_id=1).all()
    return render_template("crashes_show.html", results=results_list, targets=target_list)


@crashes.route('/crashes/all')
def all_crashes():
    return render_template("crashes_all.html", values=request.values)


@crashes.route('/crashes/show/<int:crash_id>')
def show_crash(crash_id):
    # TODO only show diff if mutation engine is external and actually mutates something (e.g. radamsa)
    # TODO Do not show diff if mutation engine is fuzzer interal or generates samples like /dev/urandom
    crash = Crash.query.get(crash_id)
    if not crash:
        flash('No crash with crash id %s' % crash_id)
        return redirect('/crashes/all')
    encoded_original_test_case, encoded_crash_test_case  = get_original_and_crash_test_case_of_crash(crash)
    return render_template("crashes_view.html",
                           crash=crash,
                           encoded_crash_test_case=encoded_crash_test_case,
                           encoded_original_test_case=encoded_original_test_case)


def get_job_name_of_job_id(job_id):
    return Job.query.get(job_id).name
    # return list(Job.objects(id=job_id))[0]["name"]


@crashes.route('/crashes/search', methods=['GET', 'POST'])
@login_required
def search_crash(error=None):
    crash_database_structure = Crash.__table__.columns.keys()
    jobs = [{'id': j.id, 'name': j.name} for j in Job.query.all()]
    # crash_database_structure = [ item for item in Crash._get_collection().find()[0]]
    # job_names =_get_job_names_of_user()
    if request.method == 'POST':
        try:
            crashes = process_search_query()
            if crashes:
                return show_crashes(crashes=crashes)
            else:
                error = "No Crashes are fitting to your Search Request"
        except Exception as e:
            error = e

    return render_template("crashes_search.html", database_structure=crash_database_structure, jobs=jobs, error=error)


def process_search_query():
    # TODO: convert from mongo
    # TODO: need better SQL search
    query = json.loads(request.form['search'])
    selected_job_name = request.form.get('job')
    try:
        dict_filter = eval("dict({})".format(query))  # noqa F481
    except ValueError:
        flash("Improper search query.")
        return list()
    if selected_job_name != "":
        job_id = _get_job_id_of_job_name(selected_job_name)
        query.update({"job_id": job_id})
    crashes = Crash.objects.aggregate(*[{"$match": query}])
    return list(crashes)


def testcase_can_be_diffed(job_id):
    return True
    mutation_engine = Job.query.get(job_id).mutation_engine
    # mutation_engine = list(Job.objects(id=job_id))[0]["mutation_engine"]
    if mutation_engine == "radamsa":
        return True
    else:
        return False


def get_original_and_crash_test_case_of_crash(crash):
    crash_test_case = crash.test_case
    original_test_case = Job.query.get(crash.job_id).seeds
    if (original_test_case.startswith(b'PK')):
        original_test_case = get_original_crash_test_case_of_zipfile(crash_test_case, original_test_case)
    # original_test_case = list(Job.objects(id=crash.job_id))[0]["samples"

    encoded_original_test_case = base64.b64encode(original_test_case[:2048]).decode('ascii')
    encoded_crash_test_case = base64.b64encode(crash_test_case[:4096]).decode('ascii')

    return encoded_original_test_case, encoded_crash_test_case


def get_original_crash_test_case_of_zipfile(crash_test_case, original_test_case):
    import zipfile
    zipfile = zipfile.ZipFile(io.BytesIO(original_test_case))
    max_similarity = 0
    for name in zipfile.namelist():
        possible_original_test_case = zipfile.read(name)
        similarity = SequenceMatcher(None, base64.b64encode(possible_original_test_case[:2048]),
                                     base64.b64encode(crash_test_case[:2048])).ratio()
        if similarity > max_similarity:
            max_similarity = similarity
            original_test_case = possible_original_test_case
    return original_test_case


@crashes.route('/crashes/show/next/<crash_id>')
def show_next_crash(crash_id):
    crash = Crash.query.get(crash_id)
    jid = crash.job_id
    next_crash = Crash.query.filter(Crash.job_id == jid, Crash.id > crash_id).first()
    if next_crash:
        return show_crash(next_crash.id)
    else:
        flash('No more crashes for job')
        return redirect('/crashes/show')


@crashes.route('/crashes/show/previous/<crash_id>')
def show_previous_crash(crash_id):
    crash = Crash.query.get(crash_id)
    jid = crash.job_id
    prev_crash = Crash.query.filter(Crash.job_id == jid, Crash.id < crash_id)  \
                            .order_by(Crash.id.desc()).first()
    if prev_crash:
        return show_crash(prev_crash.id)
    else:
        flash('No previous crashes for job')
        return redirect('/crashes/show')


@crashes.route('/crashes/delete/<int:crash_id>')
@login_required
def api_delete_crash(crash_id):
    if not delete_crash(crash_id):
        logger.error('User %s cannot delete crash with id %d',
                     current_user.email, crash_id)
        flash('You are not allowed to delete this crash.')
    else:
        flash('Crash %d deleted.' % crash_id)
    return redirect('/crashes/all')


@crashes.route('/crashes/download/<crash_id>')
@login_required
def download_crash(crash_id):
    crash = Crash.query.get(crash_id)
    if not crash:
        flash('Unknown crash ID: %s' % crash_id)
        return redirect('/crashes/all')
    filename = os.path.join('/tmp', 'flask_send_file')
    with open(filename, 'wb') as f:
        f.write(crash.test_case)
    return send_file(filename,
                     as_attachment=True,
                     attachment_filename=crash.filename)


@crashes.route('/crashes/verify', methods=["POST"])
@login_required
def verify_crash():
    token = request.values.get('r0_api_token')
    crash_id = int(request.values.get('crash_id'), 10)
    if crash_id:
        crash = Crash.query.get(crash_id)
    if token and crash:
        local_verify_crash.submit(crash_id, token)
        flash('Submitted crash id: %s' % crash_id)
    else:
        flash('Failed to submit crash id: %s' % crash_id)
    return redirect("/crashes/show/{}".format(crash_id))
