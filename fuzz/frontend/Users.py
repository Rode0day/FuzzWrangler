import random
import string
import datetime
import logging
from flask import Blueprint, render_template, request, abort, redirect
from flask_security import login_required, current_user, utils, SQLAlchemyUserDatastore, roles_required

from fuzz.database import db
from fuzz.database.models import User
from fuzz.database.models import Role

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

users = Blueprint('users', __name__)
user_datastore = SQLAlchemyUserDatastore(db, User, Role)


API_KEY_LENGTH = 32


def generate_api_key():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(API_KEY_LENGTH))


def correct_password(email, password):
    # user = User.objects.get(email=email)
    user = user_datastore.get_user(email)
    return utils.verify_password(password, user.password)


def change_password(email, new_password):
    # user = User.objects.get(email=email)
    user = user_datastore.get_user(email)
    user.password = utils.hash_password(new_password)
    db.session.commit()


def is_valid_email(email):
    return '@' in email


def is_valid_password(password):
    # TODO improve me
    return len(password) > 5


def user_exists(email):
    user = user_datastore.get_user(email)
    return user is not None


@users.route('/user/profile', methods=['GET', 'POST'])
@login_required
def show_user_profile():
    if request.method == 'GET':
        return render_template("user_profile.html", user=current_user)
    else:
        new_password = request.form['new_password']
        new_password_confirm = request.form['new_password_confirm']
        old_password = request.form['old_password']
        if new_password != new_password_confirm:
            logger.warning('Error: new password did not match')
        elif not correct_password(current_user.email, old_password):
            logger.warning('Error: wrong password')
        else:
            change_password(current_user.email, new_password)
            logger.warning('password change successful')
        return render_template("user_profile.html", user=current_user)


@users.route('/user/manage')
@roles_required('admin')
def manage_user():
    return render_template("user_manage.html", users=User.query.all())


@users.route('/user/add', methods=['GET', 'POST'])
@roles_required('admin')
def add_user():
    if request.method == 'GET':
        return render_template("user_add.html", roles=Role.query.all())
    else:
        data = request.form
        if not is_valid_email(data.get('email')):
            logger.error('Invalid email address.')
            return redirect('user/manage')

        if user_exists(data.get('email')):
            logger.error('User with email %s does already exist.' % data.get('email'))
            return redirect('user/manage')

        if data.get('password') != data.get('retype_password'):
            logger.error('Passwords do not match.')
            return redirect('user/manage')

        requested_role = Role.query.filter(Role.name == data.get('role')).one_or_none()
        if not requested_role:
            logger.error('Role %s not known' % data.get('role'))
            return redirect('user/manage')

        user_datastore.create_user(email=data.get('email'),
                                   name=data.get('name'),
                                   password=data.get('password'),
                                   api_key=generate_api_key(),
                                   roles=[requested_role],
                                   registration_date=datetime.datetime.now())
        db.session.commit()

        logger.warning('Created user %s' % data.get('email'))
        return redirect('user/manage')


@users.route('/user/delete/<user_id>')
@roles_required('admin')
def delete_user(user_id):
    if user_id is None:
        abort(400, description='Invalid user_id')
    else:
        user = user_datastore.get_user(user_id)
        if user and user != current_user:
            user_datastore.delete_user(user)
            db.session.commit()
            # TODO delete all jobs of user
            logger.warning("Deleted user with user_id %s" % user_id)
        return redirect('user/manage')


@users.route('/user/activation/<user_id>')
@roles_required('admin')
def toggle_user_activation(user_id):
    if user_id is None:
        abort(400, description='Invalid user_id')
    else:
        user = user_datastore.get_user(user_id)
        if user and user != current_user:
            user_datastore.toggle_active(user)
            db.session.commit()
            logger.warning("Set activation of user with user_id %s to %r" % (user_id, user.active))
        return redirect('user/manage')
