import logging
import flask
from sqlalchemy import func
from flask_security import login_required, current_user
from fuzz.database import db
from fuzz.database.models import Stat, Crash, Job

# from io import BytesIO
# import json
# import pandas as pd
# import plotly
# import plotly.graph_objs as go

stats = flask.Blueprint('stats', __name__)


@stats.route("/stats")
def stats_show():
    # stats = Stat.query.order_by(Stat.start_time.desc()).all()
    return flask.render_template("fuzzer_stats.html", values=flask.request.values)


@stats.route("/stats/targets")
def targets_show():
    return flask.render_template("target_stats.html", values=flask.request.values)


@stats.route("/stats/jobs")
def jobs_show():
    return flask.render_template("job_stats.html", values=flask.request.values)


@stats.route("/stats/fuzzers")
def fuzzer_targets_show():
    return flask.render_template("fuzzer_target_stats.html", values=flask.request.values)


@stats.route("/stats/show/<int:stat_id>")
def show_stat(stat_id):
    stats = Stat.query.get(stat_id)
    stats_dict = stats.to_dict(show=['job'], _hide=['job.seeds', 'plot_data', 'fuzz_bitmap'])
    stats_dict['unique_bugs'] = db.session.query(Crash.bug_id).filter(
        Crash.s_id == stat_id,
        Crash.bug_id > 0).group_by(Crash.bug_id).count()
    if stats.fuzzer_console:
        console = bytes.fromhex(stats_dict.pop('fuzzer_console')[2:]).decode('utf-8', errors='ignore')
    else:
        console = None
    # df = pd.read_csv(BytesIO(stats.plot_data))
    # df['date'] = pd.to_datetime(df['# unix_time'], unit='s')
    # gdata = [Line( x=df['unix_time'], y=df['paths_total'])]
    # ['# unix_time', ' cycles_done', ' cur_path', ' paths_total',
    #  ' pending_total', ' pending_favs', ' map_size', ' unique_crashes',
    #  ' unique_hangs', ' max_depth', ' execs_per_sec']
    # gdata = [go.Scatter(x=df['date'], y=df[' unique_crashes']) ]
    # graph_values = json.dumps(gdata, cls=plotly.utils.PlotlyJSONEncoder)

    return flask.render_template("fuzzer_view.html",
                                 stats=stats_dict,
                                 console=console)


@stats.route("/stats/plot_data/<int:stat_id>")
def get_plot_data(stat_id):
    stat = Stat.query.get(stat_id)
    if stat is not None and stat.plot_data is not None:
        return stat.plot_data
    return b""
#   return b"# unix_time, cycles_done, cur_path, paths_total, pending_total, pending_favs, map_size, unique_crashes, unique_hangs, max_depth, execs_per_sec"  # noqa


@stats.route("/stats/delete/<int:stat_id>", methods=['GET', 'POST'])
@login_required
def delete_stat(stat_id):
    stat = Stat.query.get(stat_id)
    if stat and current_user.has_role('admin'):
        try:
            db.session.delete(stat)
            db.session.commit()
        except Exception as e:
            logging.error("delete_stat error: {}".format(e))
        flask.flash('Fuzzer stat {} deleted.'.format(stat_id))
    return flask.redirect('/stats')


def get_col(column, action):
    result = db.session.query(action(column)).all()
    return result[0][0]


def calculate_general_statistics():
    ex_crashes = db.session.query(
        Crash.crash_hash).filter(Crash.exploitability == 'EXPLOITABLE'
                                 ).group_by(Crash.crash_hash).count()
    executions = get_col(Job.total_execs, func.sum)
    runtime = get_col(Job.total_runtime, func.sum)
    return {'executions': executions,
            'runtime': runtime / (60 * 60),
            'execs_per_sec': float(executions) / runtime,
            'number_of_jobs': Job.query.count(),
            'number_of_crashes': get_col(Stat.unique_crashes.distinct(), func.sum),
            'number_of_unique_crashes': get_col(Crash.crash_hash.distinct(), func.count),
            'number_of_exploitable_crashes': ex_crashes}


def CalculateStatistics():
    stats = dict()
    stats['general'] = calculate_general_statistics()
    stats['different_crash_signals'] = dict()
    return stats


@stats.route('/statistics/show', methods=['GET'])
def show_statistics():
    statistics = CalculateStatistics()
    return flask.render_template('stats_show.html', statistics=statistics)
