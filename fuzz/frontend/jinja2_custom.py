from datetime import datetime

COLORS = {'EXPLOITABLE': 'red',
          'PROBABLY_EXPLOITABLE': 'orange',
          'PROBABLY_NOT_EXPLOITABLE': 'green'}


def exploitable_color(s):
    if s is None:
        return "black"
    s = s.upper()
    if s in COLORS:
        return COLORS[s]
    else:
        return "black"


def map_signal_to_string(signal):
    if type(signal) is int:
        signal = str(signal)
    signal_mapping = {
        "129": "SIGHUP(129)",
        "132": "SIGILL(132)",
        "134": "SIGABRT(134)",
        "136": "SIGFPE(136)",
        137: "SIGKILL(137)",
        139: "SIGSEGV(139)",
        "139": "SIGSEGV(139)",
        "140": "SIGSYS(140)",
        "143": "SIGTERM(143)",
        "146": "SIGCONT/SIGSTOP/SIGCHILD(146)",
        "159": "SIGSYS(159)"
    }
    if signal in signal_mapping:
        signal = signal_mapping[signal]
    return signal


def friendly_time(dt, past_="ago",
                  future_="from now",
                  default="just now"):
    """
    Returns string representing "time since"
    or "time until" e.g.
    3 days ago, 5 hours from now etc.
    """

    if not isinstance(dt, datetime):
        return "null"

    now = datetime.utcnow()
    if now > dt:
        diff = now - dt
        dt_is_past = True
    else:
        diff = dt - now
        dt_is_past = False

    periods = (
        (diff.days / 365, "year", "years"),
        (diff.days / 30, "month", "months"),
        (diff.days / 7, "week", "weeks"),
        (diff.days, "day", "days"),
        (diff.seconds / 3600, "hour", "hours"),
        (diff.seconds / 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    )

    for period, singular, plural in periods:

        if period:
            return "%d %s %s" % (period,
                                 singular if period == 1 else plural,
                                 past_ if dt_is_past else future_)

    return default


#  provides time-ago functionality
def datetime_to_str(dt, end=datetime.utcnow(), append=""):
    if not isinstance(dt, datetime):
        return "null"
    td = end - dt
    d = td.days
    if d > 365:
        return "-"
    m, s = divmod(td.seconds, 60)
    h, m = divmod(m, 60)
    s = "{} hr, {} min".format(h, m)
    if d > 0:
        s = "{} days, ".format(d) + s
    return s + append


def bin_to_hex(data):
    return data.hex()
