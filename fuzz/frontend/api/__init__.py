
from flask import Blueprint

jobs_api = Blueprint('job_api', __name__)
crash_api = Blueprint('crash_api', __name__)
stats_api = Blueprint('stats_api', __name__)


from fuzz.frontend.api import StatsApi
from fuzz.frontend.api import JobsApi
from fuzz.frontend.api import CrashesApi
