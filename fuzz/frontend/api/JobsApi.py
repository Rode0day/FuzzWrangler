import hashlib
import json
import yaml
import datetime
import logging
from flask import request, jsonify
from flask_security import auth_token_required, current_user
from sqlalchemy import func

from fuzz.database import db
from fuzz.database.models import Job, JobSchema
from fuzz.database.models import Fuzzer, Verifier, Target, Stat, Crash
from fuzz.frontend.api import jobs_api

from marshmallow import ValidationError, EXCLUDE

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def is_job_admin(owner):
    return current_user.has_role('admin') or current_user.email == owner.email


def delete_job(job_id):
    job = Job.query.get(job_id)
    if job and is_job_admin(job.owner):
        try:
            db.session.delete(job)
            db.session.commit()
            return True
        except Exception as e:
            logger.error("delete_job error: {}".format(e))
    return False


def add_job(new_job):
    try:
        db.session.add(new_job)
        db.session.commit()
        return True
    except Exception as e:
        logger.error("add_job error: {}".format(e))
    return False


def sha1digest(blob):
    m = hashlib.sha1()
    m.update(blob)
    return m.digest()


def api_get_job_information(job):
    return {'name': job.name,
            'mutation_engine': job.mutation_engine,
            'job_id': str(job.id),
            'description': job.description,
            'maximum_samples': job.maximum_samples,
            'maximum_iteration': job.maximum_iteration,
            'archived': job.archived,
            'duration': job.duration,
            'enabled': job.enabled,
            'fuzzer': job.fuzzer.name,
            'verifier': job.verifier.name,
            'cmd_args': job.cmd_args
            }


@jobs_api.route('/api/jobs', methods=['GET'])
def api_get_jobs():
    res = Job.query.all()
    return jsonify([api_get_job_information(job) for job in res])


@jobs_api.route('/api/jobs_show', methods=['GET'])
def api_get_jobs_all():
    cols = [Job.id.label('job_id'),
            Job.name.label('job_name'),
            Job.total_execs,
            Job.total_runtime,
            Job.date.label('job_date'),
            Fuzzer.name.label('fuzzer_name'),
            func.count(Stat.id.distinct()).label('num_fuzzers'),
            func.count(Crash.id.distinct()).label('total_crashes'),
            func.count(Crash.bug_id.distinct()).label('total_bugs'),
            func.sum(Stat.execs_done.distinct()).label('total_executions'),
            func.avg(Stat.execs_per_sec).label('speed'),
            func.max(Stat.paths_found.distinct()).label('total_found'),
            func.max(Stat.paths_total.distinct()).label('total_paths'),
            func.max(Stat.pending_favs.distinct()).label('total_pfav'),
            func.max(Stat.pending_total.distinct()).label('total_pending'),
            Job.description]
    base_q = db.session.query(*cols).\
        join(Job, Fuzzer.jobs).join(Stat, Job.stats).outerjoin(Crash, Job.crashes).\
        group_by(Job.fuzzer_id)

    if request.values.get('target_id'):
        targetID = request.values.get('target_id')
        base_q = base_q.filter(Job.target_id == targetID)
    elif request.values.get('target_name'):
        target_name = request.values.get('target_name')
        targets = db.session.query(Target.id).filter_by(name=target_name).all()
        if targets:
            tgt_ids = [t[0] for t in targets]
            base_q = base_q.filter(Job.target_id.in_(tgt_ids))
    if request.values.get('fuzzer_id'):
        fuzzerID = request.values.get('fuzzer_id')
        base_q = base_q.filter(Job.fuzzer_id == fuzzerID)

    stats = base_q.group_by(
        Job.id, Job.name, Job.total_execs, Job.total_runtime, Job.date, Job.description, Fuzzer.name).\
        order_by(Job.date.desc()).all()
    return jsonify({'success': True, 'data': stats})


@jobs_api.route('/api/jobs/fuzzers')
# @auth_required('token','session')
def get_all_stats():
    cols = [Stat.id, Stat.unique_crashes, Stat.execs_per_sec, Stat.execs_done, Stat.bitmap_cvg,
            Stat.target_mode, Stat.cycles_done, Stat.paths_found, Stat.start_time, Stat.afl_banner,
            func.extract('epoch', Stat.last_crash).label('e_last_crash'),
            func.extract('epoch', Stat.last_update).label('e_last_update'),
            func.extract('epoch', Stat.start_time).label('e_start_time'),
            func.extract('epoch', Stat.last_path).label('e_last_path'),
            Job.id.label('job_id'), Job.name.label('job_name'), Job.description]
    stats = db.session.query(*cols).\
        join(Job, Stat.job).order_by(Stat.start_time.desc()).all()
    data = [s._asdict() for s in stats]
    return jsonify({'success': True, 'data': data})


@jobs_api.route('/api/job/<job_name>', methods=['GET'])
@jobs_api.route('/api/job/<int:job_id>', methods=['GET'])
@auth_token_required
def api_get_job(job_name=None, job_id=None):
    if job_name:
        job = Job.query.filter_by(name=job_name).first()
    if job_id:
        job = Job.query.get(job_id)
    if job:
        return jsonify(api_get_job_information(job))
    return jsonify({'success': False, 'msg': 'unknown job'})


@jobs_api.route('/api/job/<int:job_id>', methods=['DELETE'])
@auth_token_required
def api_delete_job(job_id=None):
    if job_id is None:
        return jsonify({'success': False, 'msg': 'no job ID provided'})
    if delete_job(job_id):
        return jsonify({'success': True})
    else:
        return jsonify({'success': False})


@jobs_api.route('/api/job/update/<int:job_id>', methods=['POST'])
@auth_token_required
def api_update_job(job_id=None):
    req = request
    if req.is_json:
        data = req.get_json()
        secret = data.get('job_secret')
    else:
        data = json.load(req.files['json']) if 'json' in req.files else {}
        secret = data.get('job_secret', req.values.get('job_secret'))
    job = Job.query.filter_by(job_secret=secret).first()
    if job is None or job.id != job_id:
        return jsonify({'success': False, 'message': 'Job not found.'}), 400
    if len(data) == 0:
        return jsonify({'success': False, 'message': 'Failed to parse job data'}), 400
    try:
        data['job_secret'] = secret
        JobSchema().load(data, partial=True, unknown=EXCLUDE)
    except ValidationError as err:
        logger.error('Validation error (update_job): %s', err.messages)

    if 'plot_data' in req.files:
        job.plot_data = req.files['plot_data'].read()

    for k, v in data.items():
        if hasattr(job, k):
            setattr(job, k, v)

    db.session.commit()
    return jsonify({'success': True,
                    'job_id': job.id,
                    'message': 'job updated'}), 200


@jobs_api.route('/api/job', methods=['POST'])
@auth_token_required
def api_create_job():
    # TODO check if a job with this name does already exist
    req = request
    data = json.load(req.files['json']) if 'json' in req.files else None
    if data is None:
        data = yaml.load(req.files['yaml']) if ('yaml' in req.files) else None
    target_file = req.files.get('target', None)
    seed_files = req.files.get('seeds', None)
    target_src = req.files['src_archive'].read() if 'src_archive' in req.files else None
    fw_file = req.files['firmware'].read() if 'firmware' in req.files else None
    if data is None:
        return jsonify({'success': False, 'msg': 'json file not provided'})
    # TODO sanitize data
    try:
        data['job_secret'] = req.values.get('job_secret')
        JobSchema().load(data, unknown=EXCLUDE)
    except ValidationError as err:
        logger.error('Validation error (create_job): {}'.format(err.messages))
        logger.warning('RECd:  {}'.format(data))
        return jsonify({'success': False, 'msg': err.messages}), 422
    if target_file is None and data.get('target_hash', None) is None:
        return jsonify({'success': False, 'msg': 'target file/hash not provided'})
    for subcls in ['target_info', 'fuzzer_info', 'verifier']:
        if subcls not in data:
            return jsonify({
                'success': False,
                'msg': '{} metadata missing'.format(subcls)})

    target_info = data.get('target_info')
    if target_file is not None:
        target_filename = target_file.filename
        target_filedata = target_file.stream.read()
        target_filehash = sha1digest(target_filedata)
    else:
        target_filename = target_info.get('filename', 'dummy')
        target_filedata = b'NULL'
        target_filehash = bytes.fromhex(data.get('target_hash'))

    new_target = Target.get_or_create(
        bin_hash=target_filehash,
        _defaults={
            'name': target_info.get('name'),
            'filename': target_filename,
            'source': target_info.get('source'),
            'binary': target_filedata,
            'bin_hash': target_filehash,
            'challenge_id': target_info.get('challenge_id'),
            'architecture': target_info.get('architecture'),
            'src_archive': target_src,
            'known_bugs': target_info.get('known_bugs')
        })

    f_defaults = {'id': data.get('fuzzer_info').get('id'),
                  '_defaults': data.get('fuzzer_info')}
    job_fuzzer = Fuzzer.get_or_create(**f_defaults)

    v_defaults = {'id': data.get('verifier').get('id'),
                  '_defaults': data.get('verifier')}
    job_verifier = Verifier.get_or_create(**v_defaults)

    new_job = Job(name=data.get('name'),
                  job_secret=data.get('job_secret'),
                  description=data.get('description'),
                  maximum_samples=data.get('maximum_samples'),
                  maximum_iteration=data.get('maximum_iteration'),
                  duration=int(data.get('duration')),
                  date=datetime.datetime.now(),
                  mutation_engine=data.get('mutation_engine'),
                  fuzzer=job_fuzzer,
                  seeds=seed_files.read() if seed_files else None,
                  fuzzing_target=new_target,
                  cmd_args=data.get('cmdline'),
                  verifier=job_verifier,
                  firmware_root=fw_file,
                  owner=current_user)
    if add_job(new_job):
        return jsonify({
            'success': True,
            'job_id': new_job.id,
            'target_id': new_job.target_id,
            'fuzzer_id': new_job.fuzzer_id,
            'verifier_id': new_job.verifier_id,
        })
    return jsonify({'success': False, 'msg': 'database error'})
