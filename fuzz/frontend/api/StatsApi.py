import logging
from subprocess import Popen, PIPE
from datetime import datetime
from flask import request, jsonify
from flask_security import auth_token_required, auth_required  # noqa
from sqlalchemy import func
from marshmallow import ValidationError, EXCLUDE

from fuzz.database import db
from fuzz.database.models import Job, Crash, Target, Fuzzer
from fuzz.database.models import Stat, StatSchema
from fuzz.frontend.api import stats_api


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

stats_schema = StatSchema()


def parse_afl_stats_file(fs):
    data = fs.read(8192)
    res = dict()
    for l in data.decode('utf-8').strip().split('\n'):
        if ' : ' not in l:
            continue
        k, v = l.strip(" %").split(':', 1)
        if 'last' in k or 'start_time' in k:
            try:
                v = datetime.utcfromtimestamp(int(v))
            except ValueError:
                v = datetime.utcfromtimestamp(0)
        res[k.strip()] = v
    return res


@stats_api.route('/api/stats')
# @auth_required('token','session')
def get_all_stats():
    cols = [Stat.id, Stat.unique_crashes, Stat.execs_per_sec, Stat.execs_done, Stat.bitmap_cvg,
            Stat.target_mode, Stat.cycles_done, Stat.paths_found, Stat.start_time, Stat.afl_banner,
            Stat.last_update, Stat.last_path, Stat.alive,
            func.extract('epoch', Stat.last_update).label('e_last_update'),
            func.extract('epoch', Stat.start_time).label('e_start_time'),
            func.extract('epoch', Stat.last_path).label('e_last_path'),
            Job.id.label('job_id'), Job.name.label('job_name'), Job.description]
    base_q = db.session.query(*cols).join(Job, Stat.job)
    if request.values.get('target_id'):
        targetID = request.values.get('target_id')
        base_q = base_q.filter(Job.target_id == targetID)
    elif request.values.get('target_name'):
        target_name = request.values.get('target_name')
        targets = db.session.query(Target.id).filter_by(name=target_name).all()
        if targets:
            tgt_ids = [t[0] for t in targets]
            base_q = base_q.filter(Job.target_id.in_(tgt_ids))
    if request.values.get('fuzzer_id'):
        fuzzerID = request.values.get('fuzzer_id')
        base_q = base_q.filter(Job.fuzzer_id == fuzzerID)

    data = base_q.order_by(Stat.start_time.desc()).all()
#   data = [s._asdict() for s in stats]
    return jsonify({'success': True, 'data': data})


@stats_api.route('/api/stats/jobs')
# @auth_required('token', 'session')
def get_job_stats():
    sq_cols = [
        Crash.job_id.label('job_id'),
        func.count(Crash.crash_hash.distinct()).label('unique_crashes'),
        func.count(Crash.bug_id.distinct()).label('total_bugs'),
    ]
    sub_q = db.session.query(*sq_cols).group_by(Crash.job_id).subquery()
    cols = [Job.id.label('job_id'),
            Job.alive,
            Job.name.label('job_name'),
            Job.target_id.label('tgt_id'),
            Job.total_execs,
            Job.total_runtime,
            Job.cov_line,
            Job.cov_func,
            Job.cov_branch,
            Job.cov_bb,
            Job.cov_bbe,
            Job.mon_edges,
            Job.mon_bugs,
            Job.cov_json,
            Job.date.label('created'),
            func.sum(Stat.unique_crashes).label('crashes'),
            func.sum(Stat.paths_found).label('paths_found'),
            func.max(sub_q.c.unique_crashes).label('unique_crashes'),
            func.max(sub_q.c.total_bugs).label('total_bugs'),
            Job.description]
    base_q = db.session.query(*cols).\
        join(Job, Stat.job).\
        outerjoin(sub_q, Job.id == sub_q.c.job_id)

    if request.values.get('target_id'):
        targetID = request.values.get('target_id')
        base_q = base_q.filter(Job.target_id == targetID)
    elif request.values.get('target_name'):
        target_name = request.values.get('target_name')
        targets = db.session.query(Target.id).filter_by(name=target_name).all()
        if targets:
            tgt_ids = [t[0] for t in targets]
            base_q = base_q.filter(Job.target_id.in_(tgt_ids))
    if request.values.get('fuzzer_id'):
        fuzzerID = request.values.get('fuzzer_id')
        base_q = base_q.filter(Job.fuzzer_id == fuzzerID)

    if request.values.get('all'):
        stats = base_q.group_by(Job.id).order_by(Job.date.desc()).all()
    else:
        stats = base_q.group_by(Job.id).order_by(Job.date.desc()).limit(1000).all()

    return jsonify({'success': True, 'data': stats})


@stats_api.route('/api/stats/fuzzers/by_id')
def get_fuzzer_target_stats_id():
    c_start_time = func.max(Job.date).label('start_time')
    sq_cols = [
        Crash.job_id.label('job_id'),
        func.count(Crash.crash_hash.distinct()).label('unique_crashes'),
        func.count(Crash.bug_id.distinct()).label('total_bugs'),
    ]
    sub_q = db.session.query(*sq_cols).group_by(Crash.job_id).subquery()
    cols = [Fuzzer.id.label('fuzzer_id'),
            Fuzzer.name.label('fuzzer_name'),
            Target.id.label('tgt_id'),
            Target.name.label('tgt_name'),
            Target.known_bugs,
            func.count(Stat.id.distinct()).label('num_fuzzers'),
            func.count(Job.id.distinct()).label('n_jobs'),
            func.max(sub_q.c.unique_crashes).label('unique_crashes'),
            func.max(sub_q.c.total_bugs).label('total_bugs'),
            func.sum(Stat.unique_crashes).label('total_crashes'),
            func.sum(Stat.execs_done).label('total_execs'),
            func.avg(Stat.execs_done).label('avg_execs'),
            func.avg(Stat.paths_found).label('avg_paths'),
            func.sum(Job.total_runtime.distinct()).label('total_runtime'),
            func.max(Stat.execs_per_sec).label('max_speed'),
            func.max(Stat.bitmap_cvg).label('max_coverage'),
            func.max(Job.cov_line).label('max_l_ratio'),
            func.max(Job.cov_bb).label('max_bb'),
            func.max(Job.cov_bbe).label('max_edge'),
            func.encode(Target.bin_hash, 'hex').label('tgt_hash'),
            Target.challenge_id
            ]
    stats = db.session.query(*cols).\
        join(Job, Fuzzer.jobs).\
        join(Target, Job.fuzzing_target).join(Stat, Job.stats).\
        outerjoin(sub_q, Job.id == sub_q.c.job_id).\
        group_by(Fuzzer.id).group_by(Target.id).\
        order_by(c_start_time.desc()).all()
    return jsonify({'success': True,
                    'data': stats})


@stats_api.route('/api/stats/fuzzers/by_name')
def get_fuzzer_target_stats_name():
    c_start_time = func.max(Job.date).label('start_time')
    sq_cols = [
        Crash.job_id.label('job_id'),
        func.count(Crash.crash_hash.distinct()).label('unique_crashes'),
        func.count(Crash.bug_id.distinct()).label('total_bugs'),
    ]
    sub_q = db.session.query(*sq_cols).group_by(Crash.job_id).subquery()
    cols = [Fuzzer.id.label('fuzzer_id'),
            Fuzzer.name.label('fuzzer_name'),
            func.min(Target.id).label('tgt_id'),
            Target.name.label('tgt_name'),
            func.max(Target.known_bugs).label('known_bugs'),
            func.count(Stat.id.distinct()).label('num_fuzzers'),
            func.count(Job.id.distinct()).label('n_jobs'),
            func.max(sub_q.c.unique_crashes).label('unique_crashes'),
            func.max(sub_q.c.total_bugs).label('total_bugs'),
            func.sum(Stat.unique_crashes).label('total_crashes'),
            func.sum(Stat.execs_done).label('total_execs'),
            func.avg(Stat.execs_done).label('avg_execs'),
            func.avg(Stat.paths_found).label('avg_paths'),
            func.sum(Job.total_runtime.distinct()).label('total_runtime'),
            func.max(Stat.execs_per_sec).label('max_speed'),
            func.max(Stat.bitmap_cvg).label('max_coverage'),
            func.max(Job.cov_line).label('max_l_ratio'),
            func.max(Job.cov_bb).label('max_bb'),
            func.max(Job.cov_bbe).label('max_edge'),
            Target.name.label('tgt_hash'),
            func.min(Target.challenge_id).label('challenge_id')
            ]
    stats = db.session.query(*cols).\
        join(Job, Fuzzer.jobs).\
        join(Target, Job.fuzzing_target).join(Stat, Job.stats).\
        outerjoin(sub_q, Job.id == sub_q.c.job_id).\
        group_by(Fuzzer.id).group_by(Target.name).\
        order_by(c_start_time.desc()).all()
    return jsonify({'success': True,
                    'data': stats})


@stats_api.route('/api/stats/targets')
# @auth_required('token','session')
def get_target_stats():
    c_start_time = func.max(Stat.start_time).label('start_time')
    sq_cols = [
        Crash.job_id.label('job_id'),
        func.count(Crash.crash_hash.distinct()).label('unique_crashes'),
        func.count(Crash.bug_id.distinct()).label('total_bugs'),
    ]
    sub_q = db.session.query(*sq_cols).group_by(Crash.job_id).subquery()
    cols = [Target.id.label('tgt_id'),
            Target.name.label('tgt_name'),
            Target.filename.label('tgt_filename'),
            Target.known_bugs,
            Target.challenge_id.label('tgt_chall_id'),
            func.encode(Target.bin_hash, 'hex').label('tgt_hash'),
            func.count(Stat.id.distinct()).label('num_fuzzers'),
            func.sum(Stat.unique_crashes).label('total_crashes'),
            func.sum(Stat.unique_crashes).label('sum_crashes'),
            func.max(Stat.execs_per_sec).label('max_speed'),
            func.sum(Stat.execs_done).label('total_executions'),
            func.max(Stat.bitmap_cvg).label('max_coverage'),
            func.max(Job.cov_line).label('max_line_cov'),
            func.max(Job.cov_bb).label('max_bb_count'),
            func.max(Job.cov_bbe).label('max_edge_count'),
            func.sum(Stat.cycles_done).label('total_cycles'),
            func.sum(Stat.paths_found).label('total_paths'),
            c_start_time,
            func.count(Job.id.distinct()).label('n_jobs'),
            func.max(sub_q.c.unique_crashes).label('unique_crashes'),
            func.max(sub_q.c.total_bugs).label('total_bugs')
            ]
    stats = db.session.query(*cols).select_from(Job).\
        join(Target).join(Job.stats).\
        outerjoin(sub_q, Job.id == sub_q.c.job_id).\
        group_by(Target.id).order_by(c_start_time.desc()).all()
    # data = [dict(zip(s.keys(),s)) for s in stats]
    return jsonify({'success': True,
                    'data': stats})


@stats_api.route('/api/stats/targets/by_name')
# @auth_required('token','session')
def get_target_stats_name():
    c_start_time = func.max(Stat.start_time).label('start_time')
    sq_cols = [
        Crash.job_id.label('job_id'),
        func.count(Crash.crash_hash.distinct()).label('unique_crashes'),
        func.count(Crash.bug_id.distinct()).label('total_bugs'),
    ]
    sub_q = db.session.query(*sq_cols).group_by(Crash.job_id).subquery()
    cols = [func.min(Target.id).label('tgt_id'),
            Target.name.label('tgt_name'),
            Target.name.label('tgt_hash'),
            func.max(Target.known_bugs).label('known_bugs'),
            func.max(Target.challenge_id).label('tgt_chall_id'),
            func.count(Stat.id.distinct()).label('num_fuzzers'),
            func.sum(Stat.unique_crashes).label('total_crashes'),
            func.max(sub_q.c.unique_crashes).label('unique_crashes'),
            func.max(sub_q.c.total_bugs).label('total_bugs'),
            func.max(Stat.execs_per_sec).label('max_speed'),
            func.sum(Stat.execs_done).label('total_executions'),
            func.max(Stat.bitmap_cvg).label('max_coverage'),
            func.max(Job.cov_line).label('max_line_cov'),
            func.max(Job.cov_bb).label('max_bb_count'),
            func.max(Job.cov_bbe).label('max_edge_count'),
            func.sum(Stat.cycles_done).label('total_cycles'),
            func.sum(Stat.paths_found).label('total_paths'),
            c_start_time,
            func.count(Job.id.distinct()).label('n_jobs')]
    stats = db.session.query(*cols).\
        join(Target, Job.fuzzing_target).join(Stat, Job.stats).\
        outerjoin(sub_q, Job.id == sub_q.c.job_id).\
        group_by(Target.name).order_by(c_start_time.desc()).all()
    # data = [dict(zip(s.keys(),s)) for s in stats]
    return jsonify({'success': True,
                    'data': stats})


@stats_api.route('/api/stats/submit', methods=['POST'])
@auth_token_required
def update_stats(job_id=None):
    req = request
    secret = req.values.get('job_secret')
    job = Job.query.filter_by(job_secret=secret).first()
    if job is None:
        return jsonify({'success': False, 'message': 'Job not found.'}), 400
    if 'stats_file' in req.files:
        stats_data = parse_afl_stats_file(req.files['stats_file'])
        stats_data['id'] = req.values.get('id')
    elif req.is_json:
        stats_data = req.get_json() if req.form is None else None
    if stats_data is None:
        return jsonify({'success': False, 'message': 'No stats provided'}), 400
    # Validate and deserialize input
    try:
        stats_schema.load(stats_data, unknown=EXCLUDE)
    except ValidationError as err:
        logger.warning("Rec'd: {}".format(stats_data))
        logger.error('Validation error (update_stats): {}'.format(err.messages))
        return jsonify({'success': False, 'message': err.messages}), 422

    if 'plot_data' in req.files:
        stats_data['plot_data'] = req.files['plot_data'].read()
    if 'fuzz_bitmap' in req.files:
        stats_data['fuzz_bitmap'] = req.files['fuzz_bitmap'].read()
    if 'console.html' in req.files:
        stats_data['fuzzer_console'] = req.files['console.html'].read()
    elif 'console.log' in req.files:
        console = req.files['console.log'].read().strip()
        p = Popen('ansi2html.sh', stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = p.communicate(console)
        stats_data['fuzzer_console'] = out

    stats_data['fuzzer_hostname'] = req.values.get('fuzzer_hostname')
    stats_data['job_id'] = job.id
    stats_data['alive'] = (req.values.get('alive') == '1')
    sid = req.values.get('stat_id')
    s_defaults = {'id': sid,
                  '_defaults': stats_data,
                  '_session': db.session}
    new_stat, created = Stat._get_or_create(**s_defaults)
    if created is False:
        new_stat.from_dict(**stats_data)
    db.session.commit()
    return jsonify({'success': True,
                    'stat_id': new_stat.id,
                    'job_id': job.id,
                    'message': 'stats sucessfully submitted'}), 200
