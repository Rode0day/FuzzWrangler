from binascii import unhexlify
import requests
import io
import logging
import time
import yaml
from flask import request, jsonify, send_file
from flask_security import auth_token_required, auth_required
from marshmallow import ValidationError
from sqlalchemy import func

from fuzz.database import db
from fuzz.database.models import Job, Stat, Target
from fuzz.frontend.app import executor
from fuzz.frontend.api import JobsApi, crash_api
from fuzz.database.models import Crash, CrashSchema

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

crash_schema = CrashSchema()


@executor.job
def local_verify_crash(crash_id, api_token):
    # db.create_engine('dialect+driver://user:pass@host:port/db')
    # verify_rode0day.delay(crash_id, api_token)
    logger.info("Verify crash id: %d", crash_id)
    try:
        crash = db.session.query(Crash).get(crash_id)
        API_URL = "https://rode0day.mit.edu/api/1.0/submit"
        data = {'challenge_id': crash.job.fuzzing_target.challenge_id,
                'auth_token': api_token}
        files = {'input': (crash.filename, crash.test_case)}
        r = requests.post(API_URL, data=data, files=files)
        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            error = yaml.load(r.text)
            logger.warning("API Error %d: %s", error["status"], error["status_str"])
            if error["status"] == 6:
                logger.warning("Sleeping for a minute...")
                time.sleep(60)
                return local_verify_crash(crash_id, api_token)
            else:
                return None

        result = yaml.safe_load(r.text)
        if result['status'] == 0 and len(result['bug_ids']):
            crash.bug_id = result['bug_ids'][0]
            crash.verified = 1
        else:
            crash.verified = -1
        if 'first_ids' in result and len(result['first_ids']) > 0:
            crash.additional = "{}\n{} {}".format(crash.additional,
                                                  "FIRST: ",
                                                  ', '.join(map(str, result['first_ids'])))
        else:
            crash.additional = "{}\n{}".format(crash.additional,
                                               result['status_s'])
        db.session.commit()
        logger.info("SENT %s --> %s", crash.filename, result['status_s'])
    finally:
        db.session.remove()


@crash_api.route('/api/crash')
def api_get_all_crashes():
    cols = [Job.id.label('job_id'),
            Job.name.label('job_name'),
            Job.fuzzer_id.label('fuzzer_id'),
            Stat.id.label('stat_id'),
            Stat.afl_banner.label('fuzzer_name'),
            Target.id.label('tgt_id'),
            Target.name.label('tgt_name')]
    subq = db.session.query(*cols).join(Stat, Job.stats).join(Target, Job.target_id == Target.id).subquery()
    cols = [Crash.id,
            subq.c.job_id,
            subq.c.job_name,
            subq.c.stat_id,
            subq.c.fuzzer_name,
            subq.c.tgt_id,
            subq.c.tgt_name,
            Crash.verified,
            Crash.bug_id,
            Crash.filename,
            Crash.iteration,
            Crash.bb_count,
            Crash.bb_count_mu,
            Crash.crash_signal,
            func.octet_length(Crash.test_case).label('test_case_size'),
            Crash.exploitability,
            func.encode(Crash.crash_hash, 'hex').label('crash_hash'),
            Crash.timestamp]
    base_q = db.session.query(*cols).join(subq, Crash.s_id == subq.c.stat_id)

    if request.values.get('job_id'):
        job_id = request.values.get('job_id')
        base_q = base_q.filter(subq.c.job_id == job_id)
    if request.values.get('fuzzer_id'):
        fuzzer_id = request.values.get('fuzzer_id')
        base_q = base_q.filter(subq.c.fuzzer_id == fuzzer_id)
    if request.values.get('target_id'):
        target_id = request.values.get('target_id')
        base_q = base_q.filter(subq.c.tgt_id == target_id)
    elif request.values.get('target_name'):
        target_name = request.values.get('target_name')
        base_q = base_q.filter(subq.c.tgt_name == target_name)
    if request.values.get('bug_id'):
        bug_id = request.values.get('bug_id')
        base_q = base_q.filter(Crash.bug_id == bug_id)
    if request.values.get('verified') == '1':
        base_q = base_q.filter(Crash.verified > 0)
    elif request.values.get('verified') == '0':
        base_q = base_q.filter(Crash.verified < 1)
    if request.values.get('uniqbugs') == '1':
        base_q = base_q.order_by(Crash.bug_id, subq.c.job_id, Crash.timestamp)
        base_q = base_q.distinct(Crash.bug_id)
    elif request.values.get('nullbugs') == '1':
        base_q = base_q.filter(Crash.bug_id == None)    # noqa

    base_q = base_q.from_self().order_by(Crash.timestamp.desc())
    crashes = base_q.limit(1000).all()

    data = [c._asdict() for c in crashes]
    return jsonify({'success': True, 'data': data}), 200


@crash_api.route('/api/crash/<int:stat_id>')
@auth_token_required
def api_get_crashes(stat_id):
    cols = [Crash.filename,
            func.encode(Crash.crash_hash, 'hex').label('hash'),
            Crash.verified]
    crashes = db.session.query(*cols).filter(Crash.s_id == stat_id).all()
    if not crashes or len(crashes) == 0:
        return jsonify({"success": False, "crashes": []}), 200
    data = [c._asdict() for c in crashes]
    return jsonify({"success": True, "crashes": data}), 200


@crash_api.route('/api/crash/reverify', methods=['POST'])
@auth_required('token', 'session')
def api_reverify_crash():
    token = request.values.get('r0_api_token')
    crash_id = int(request.values.get('crash_id'), 10)
    if crash_id:
        crash = Crash.query.get(crash_id)
    if token and crash:
        local_verify_crash.submit(crash_id, token)
        return jsonify({'success': True}), 200
    return jsonify({"success": False}), 200


@crash_api.route('/api/crash/verify', methods=['POST'])
@auth_token_required
def api_verify_crash():
    values = request.values
    if 'target_hash' not in values and 'challenge_id' not in values:
        return jsonify({'success': False,
                        'message': 'No metadata provided.'}), 400
    limit = int(values.get('limit')) if 'limit' in values else 10
    bug_id = True if 'bug_id' in values else False
    coverage = True if 'coverage' in values else False
    verified = int(values.get('verified')) if 'verified' in values else False
    cols = [Crash.id.label('crash_id'),
            Crash.s_id.label('stat_id'),
            Crash.additional,
            Target.name,
            Target.challenge_id,
            func.encode(Target.bin_hash, 'hex').label('target_hash'),
            func.encode(Crash.crash_hash, 'hex').label('crash_hash')]
    base_q = db.session.query(*cols).join(Job, Crash.job).join(Target, Job.fuzzing_target)

    if 'target_hash' in values:
        bin_hash = unhexlify(values.get('target_hash'))
        base_q = base_q.filter(Target.bin_hash == bin_hash)
    if 'challenge_id' in values:
        base_q = base_q.filter(Target.challenge_id == values.get('challenge_id'))
    if bug_id:
        base_q = base_q.filter(Crash.bug_id.is_(None))
    if coverage:
        base_q = base_q.filter(Crash.bb_count.is_(None))
    if verified:
        base_q = base_q.filter(Crash.verified == verified)
    crashes = base_q.order_by(Crash.timestamp.desc()).limit(limit).all()
    if crashes is None:
        return jsonify({'success': False, 'message': 'Crash not found'}), 400
    return jsonify({'success': True, 'data': crashes}), 200


@crash_api.route('/api/crash/download/<int:crash_id>')
@auth_required('token', 'session')
def api_download_crash(crash_id):
    crash = Crash.query.get(crash_id)
    if not crash:
        return jsonify({"success": False, "message": "Crash id not found."})
    f = io.BytesIO(crash.test_case)
    return send_file(f, as_attachment=True, attachment_filename=crash.filename)


@crash_api.route('/api/crash/update', methods=['POST'])
@auth_token_required
def api_update_crash():
    data_list = request.get_json()
    if data_list is None:
        return jsonify({'success': False,
                        'message': 'Non metadata provided.'}), 400
    for data in data_list:
        if 'stat_id' not in data:
            return jsonify({'success': False,
                            'message': 'No stat_id provided.'}), 400
        sid = data.get('stat_id')
        if 'crash_hash' in data:
            crash_hash = unhexlify(data.get('crash_hash'))
            crash = Crash.query.filter(Crash.crash_hash == crash_hash,
                                       Crash.s_id == sid).first()
        if 'filename' in data:
            filename = data.get('filename')
            cnt = Crash.query.filter_by(s_id=sid, filename=filename).count()
            if cnt > 1:
                return jsonify({'success': False,
                                'message': 'Duplicate filenames for {}: {}'.format(
                                    filename, sid)}), 400
            crash = Crash.query.filter_by(s_id=sid, filename=filename).first()
        if crash is None:
            return jsonify({'success': False,
                            'message': 'Crash not found.'}), 400

        if 'bug_id' in data:
            crash.bug_id = data.get('bug_id')
            crash.verified = 1
        if 'verified' in data:
            crash.verified = data.get('verified')
            if crash.bug_id is None:
                crash.bug_id = -1
        if 'additional' in data:
            crash.additional = data.get('additional')
        if 'bb_counts' in data:
            bb_counts = data.get('bb_counts')
            crash.bb_count_a = bb_counts.get('bb_all')
            crash.be_count_mu = bb_counts.get('bbe_mu')
            crash.bb_count_ma = bb_counts.get('bb_ma')
            crash.bb_count_mu = bb_counts.get('bb_mu')
        if 'bb_count' in data:
            crash.bb_count = data.get('bb_count')
        if 'exploitability' in data:
            crash.exploitability = data.get('exploitability')
        if 'stack_hash' in data:
            crash.stack_hash_maj, crash.stack_hash_min = data['stack_hash'].split('.', 1)

        db.session.commit()
        crash = Crash.query.get(crash.id)
        if 'r0_api_token' in data and crash.bug_id is None:
            local_verify_crash.submit(crash.id, data.get('r0_api_token'))
    return jsonify({'success': True,
                    'updated': len(data_list),
                    'message': '{} crashes updated'.format(len(data_list))})


@crash_api.route('/api/crash/submit', methods=['POST'])
@auth_token_required
def api_add_crash():
    if not request.form:
        return jsonify({'success': False,
                        'message': 'No metadata provided.'}), 400
    try:
        data = crash_schema.load(request.form)
    except ValidationError as err:
        logger.error("Validation error (add_crash): {}".format(err.messages))
        return jsonify({"success": False, 'messages': err.messages}), 422
    if 'input' not in request.files:
        return jsonify({'success': False,
                        'message': 'No crash input provided.'}), 400

    secret = request.values.get('job_secret')
    job = Job.query.filter_by(job_secret=secret).first()
    if job is None:
        return jsonify({'success': False,
                        'message': 'Job not found.'}), 400
    crash_count = Crash.query.filter_by(job_id=job.id).count()
    crash_filename = request.files['input'].filename
    crash_filedata = request.files['input'].read()
    crash_filehash = JobsApi.sha1digest(crash_filedata)
    verified = data.get('verified') if 'verified' in data else 0

    crash_data = {
        'crash_signal': data.get('crash_signal'),
        'exploitability': data.get('exploitability'),
        'timestamp': data.get('timestamp'),
        'crash_hash': crash_filehash,
        'verified': verified,
        'bug_id': data.get('bug_id'),
        'filename': crash_filename,
        'additional': data.get('additional'),
        'test_case': crash_filedata,
        'iteration': crash_count + 1,
        's_id': data.get('stat_id'),
        'job_id': job.id}

    if 'stack_hash' in data:
        crash_data['stack_hash_maj'], crash_data['stack_hash_min'] = data['stack_hash'].split('.', 1)

    filter_by = {'job_id': job.id, 'crash_hash': crash_filehash}
    new_crash, created = Crash._get_or_create(_defaults=crash_data, _session=db.session, **filter_by)
    new_crash_id = new_crash.id
    db.session.commit()
    if created and 'r0_api_token' in data:
        local_verify_crash.submit(new_crash_id, data.get('r0_api_token'))
    msg = 'crash submitted' if created else 'crash exists'
    return jsonify({'success': created,
                    'crash_id': new_crash_id,
                    'message': msg,
                    }), 200
