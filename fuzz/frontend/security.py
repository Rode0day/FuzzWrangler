import datetime
from flask_security import Security, SQLAlchemyUserDatastore

from fuzz.database.models import Role
from fuzz.database.models import User
from fuzz.database.models import Fuzzer, Verifier
from fuzz.database import db

from fuzz import global_config


def has_user():
    return False


def has_role():
    return False


def create_default_user_and_roles(usr_ds):
    usr_ds.find_or_create_role(name='admin',
                               description='Administrative user of LuckyCat')
    usr_ds.find_or_create_role(name='analyst',
                               description='General LuckyCat user without admin privileges')

    # admin_role = Role.objects.get(name='admin')
    if not usr_ds.find_user(email=global_config.default_user_email):
        usr_ds.create_user(email=global_config.default_user_email,
                           name=global_config.default_user_name,
                           password=global_config.default_user_password,
                           api_key=global_config.default_user_api_key,
                           registration_date=datetime.datetime.now())
        db.session.commit()
        usr_ds.add_role_to_user(global_config.default_user_email, 'admin')
        db.session.commit()
    #                          roles=[admin_role])
    #  flash('Added default user on first request', 'success')


def _add_apikey_handler(security, user_datastore):
    @security.login_manager.request_loader
    def load_user_from_request(request):
        api_key = request.headers.get('Authorization')
        if api_key:
            return user_datastore.find_user(api_key=api_key)
        return None


def create_default_fuzzers():
    # add defaults
    if Fuzzer.query.count() == 0:
        f = Fuzzer(name='afl',
                   description='AFL is a mutator and fuzzer',
                   version='v2.52b')
        db.session.add(f)
    if Verifier.query.count() == 0:
        for verifier in global_config.verifiers:
            v = Verifier(**verifier)
            db.session.add(v)
    db.session.commit()


def add_flask_security(app):
    with app.app_context():
        app.config['SECURITY_UNAUTHORIZED_VIEW'] = '/'
        app.config['SECRET_KEY'] = global_config.secret_key
        app.config['SECURITY_PASSWORD_SALT'] = global_config.secret_key

        user_datastore = SQLAlchemyUserDatastore(db, User, Role)
        security = Security(app, user_datastore)
        # Create any database tables that don't exist yet.
        db.create_all()
        create_default_user_and_roles(user_datastore)
        _add_apikey_handler(security, user_datastore)
        create_default_fuzzers()
