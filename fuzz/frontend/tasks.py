import requests
import time
import yaml
from fuzz.database import db
from fuzz.database.models import Crash
# from fuzz.frontend.app import create_celery_app
# from fuzz.frontend.app import executor

#  celery = create_celery_app()


# @celery.task
def verify_rode0day(crash_id, api_token):
    from fuzz.frontend.fuzz_frontend import app
    from celery.utils.log import get_task_logger
    logger = get_task_logger(__name__)
    with app.app_context():
        # db.create_engine('dialect+driver://user:pass@host:port/db')
        crash = Crash.query.get(crash_id)
        API_URL = "https://rode0day.mit.edu/api/1.0/submit"
        data = {'challenge_id': crash.job.fuzzing_target.challenge_id,
                'auth_token': api_token}
        files = {'input': (crash.filename, crash.test_case)}
        r = requests.post(API_URL, data=data, files=files)
        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            error = yaml.load(r.text)
            logger.warning("API Error %d: %s", error["status"], error["status_str"])
            if error["status"] == 6:
                logger.warning("Sleeping for a minute...")
                time.sleep(60)
                return verify_rode0day(crash_id, api_token)
            else:
                return None

        result = yaml.load(r.text)
        if result['status'] == 0 and len(result['bug_ids']):
            crash.bug_id = result['bug_ids'][0]
            crash.verified = 1
        else:
            crash.verified = -1
        if 'first_ids' in result and len(result['first_ids']) > 0:
            crash.additional = "{}\n{} {}".format(crash.additional,
                                                  "FIRST: ",
                                                  ', '.join(map(str, result['first_ids'])))
        else:
            crash.additional = "{}\n{}".format(crash.additional,
                                               result['status_s'])
        db.session.commit()
        logger.info("SENT %s --> %s", crash.filename, result['status_s'])


# @executor.job
def local_verify_crash(crash_id, api_token):
    import logging
    logger = logging.getLogger(__name__)
    # db.create_engine('dialect+driver://user:pass@host:port/db')
    crash = Crash.query.get(crash_id)
    API_URL = "https://rode0day.mit.edu/api/1.0/submit"
    data = {'challenge_id': crash.job.fuzzing_target.challenge_id,
            'auth_token': api_token}
    files = {'input': (crash.filename, crash.test_case)}
    r = requests.post(API_URL, data=data, files=files)
    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError:
        error = yaml.load(r.text)
        logger.warning("API Error %d: %s", error["status"], error["status_str"])
        if error["status"] == 6:
            logger.warning("Sleeping for a minute...")
            time.sleep(60)
            return local_verify_crash(crash_id, api_token)
        else:
            return None

    result = yaml.load(r.text)
    if result['status'] == 0 and len(result['bug_ids']):
        crash.bug_id = result['bug_ids'][0]
        crash.verified = 1
    else:
        crash.verified = -1
    if 'first_ids' in result and len(result['first_ids']) > 0:
        crash.additional = "{}\n{} {}".format(crash.additional,
                                              "FIRST: ",
                                              ', '.join(map(str, result['first_ids'])))
    else:
        crash.additional = "{}\n{}".format(crash.additional,
                                           result['status_s'])
    db.session.commit()
    logger.info("SENT %s --> %s", crash.filename, result['status_s'])
