#!/usr/bin/python3
import os
from fuzz.frontend.app import create_app, db
from fuzz.database.models import User, Job, Crash, Stat

config_name = os.getenv('APP_SETTINGS')

app = create_app(config_name)
app.app_context().push()

# Configure flask shell
@app.shell_context_processor
def make_shell_context():
    return {'db': db,
            'User': User,
            'Job': Job,
            'Crash': Crash,
            'Stat': Stat}


if __name__ == "__main__":
    app.run(host='0.0.0.0')
