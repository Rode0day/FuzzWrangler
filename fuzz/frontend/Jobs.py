import datetime
import logging
import json
import os
import flask
from flask_security import login_required, current_user
from sqlalchemy import func

from fuzz import global_config
from fuzz.database import db
from fuzz.database.models import Job, Stat
from fuzz.database.models import Crash, Fuzzer, Verifier, Target
from fuzz.frontend.api import JobsApi
from fuzz.frontend.InMemoryZip import InMemoryZip

jobs = flask.Blueprint('jobs', __name__)


def mutation_engine_requires_samples(engine):
    return engine != 'external' and engine != 'urandom'


@jobs.route("/jobs/show")
def jobs_show():
    jobs = Job.query.first()
    return flask.render_template("jobs_show.html", values=flask.request.values, jobs=jobs)


def jobs_show_old():
    base_q = db.session.query(Job,
                              func.count(Stat.id.distinct()).label('num_fuzzers'),
                              func.count(Crash.id.distinct()).label('total_crashes'),
                              func.count(Crash.bug_id.distinct()).label('total_bugs'),
                              func.sum(Stat.execs_done.distinct()).label('total_executions'),
                              func.avg(Stat.execs_per_sec).label('speed'),
                              func.max(Stat.paths_found.distinct()).label('total_found'),
                              func.max(Stat.paths_total.distinct()).label('total_paths'),
                              func.max(Stat.pending_favs.distinct()).label('total_pfav'),
                              func.max(Stat.pending_total.distinct()).label('total_pending'),
                              ).join(Stat, Job.stats).outerjoin(Crash, Job.crashes)
    if flask.request.values.get('target_id'):
        targetID = flask.request.values.get('target_id')
        base_q = base_q.filter(Job.target_id == targetID)
    if flask.request.values.get('fuzzer_id'):
        fuzzerID = flask.request.values.get('fuzzer_id')
        base_q = base_q.filter(Job.fuzzer_id == fuzzerID)

    if global_config.not_protected:
        jobs = base_q.group_by(Job.id).order_by(Job.date.desc()).all()
        return flask.render_template("jobs_show.html", jobs=jobs)

    filtered_jobs = []
    if current_user and current_user.has_role('admin'):
        filtered_jobs = Job.query.all()
    else:
        filtered_jobs = Job.query.filter_by(owner=current_user.id).all()

    return flask.render_template("jobs_show.html", jobs=filtered_jobs)


@jobs.route("/jobs/fuzzers")
def jobs_fuzzers_show():
    fuzzers = Job.query.first()
    return flask.render_template("fuzzers_show.html", values=flask.request.values, fuzzers=fuzzers)


def get_job_bugs(job):
    job_id = job.id
    bugs = dict()
    bugs['all'] = Crash.query.filter(Crash.job_id == job_id,
                                     Crash.bug_id != 0
                                     ).distinct(Crash.bug_id).count()
    stats_bugs = db.session.query(Crash.s_id,
                                  func.count(Crash.crash_hash.distinct()).label('u_crashes'),
                                  func.count(Crash.bug_id.distinct()).label('bugs')
                                  ).filter(Crash.job_id == job_id).group_by(Crash.s_id).all()
    for stat in job.stats:
        bugs[stat.id] = (0, 0)
    for stat in stats_bugs:
        bugs[stat.s_id] = (stat.u_crashes, stat.bugs)
    return bugs


@jobs.route("/jobs/show/<int:job_id>")
def show_job(job_id):
    job = Job.query.get(job_id)
    if job is None:
        flask.flash("Job id: {} not found.".format(job_id))
        flask.redirect('jobs/show')
    bugs = get_job_bugs(job)
    return flask.render_template("jobs_view.html", job=job, bugs=bugs)


@jobs.route("/jobs/show/next/<int:job_id>")
def show_next_job(job_id):
    next_job = Job.query.filter(Job.id > job_id).first()
    if next_job is None:
        flask.flash("No more jobs.")
        return flask.redirect('jobs/show')
    bugs = get_job_bugs(next_job)
    return flask.render_template("jobs_view.html", job=next_job, bugs=bugs)


@jobs.route("/jobs/show/previous/<int:job_id>")
def show_previous_job(job_id):
    previous_job = Job.query.filter(Job.id < job_id).order_by(Job.id.desc()).first()
    if previous_job is None:
        flask.flash("No previous jobs.")
        return flask.redirect('jobs/show')
    bugs = get_job_bugs(previous_job)
    return flask.render_template("jobs_view.html", job=previous_job, bugs=bugs)


@jobs.route("/jobs/plot_data/<int:job_id>")
def get_job_plot_data(job_id):
    job = Job.query.get(job_id)
    if job is not None and job.plot_data is not None:
        return job.plot_data
    return b""


@jobs.route("/jobs/add", methods=['GET', 'POST'])
@login_required
def add_job():
    # TODO check if job with this name already exists
    if flask.request.method == 'GET':
        fuzzers = Fuzzer.query.all()
        verifiers = Verifier.query.all()
        engines = [x['name'] for x in global_config.mutation_engines]

        return flask.render_template("jobs_add.html",
                                     engines=engines,
                                     fuzzers=fuzzers,
                                     verifiers=verifiers)
    else:
        data = flask.request.form
        files = flask.request.files

        engine = data.get('mutation_engine')
        if data.get('fuzzer') == "afl":
            engine = 'external'

        if 'fuzzing_target' not in files:
            flask.flash('Please provide a fuzzing target.')
            return flask.redirect('/jobs/add')

        if mutation_engine_requires_samples(engine) and not ('seeds' in files):
            flask.flash('If mutation engine is not external then you must provide some initial test cases.')
            return flask.redirect('/jobs/add')

        target_file = files['fuzzing_target'].stream.read()
        target_hash = JobsApi.sha1digest(target_file)

        seed_file = files['seeds'].stream.read() if 'seeds' in files else None
        fw_file = files['firmware_root'].read() if 'firmware_root' in files else None

        new_target = Target.get_or_create(
            bin_hash=target_hash,
            _defaults={
                'name': data.get('target_name'),
                'filename': target_file.filename,
                'source': data.get('target_source'),
                'binary': target_file,
                'bin_hash': target_hash
            }
        )

        new_job = Job(name=data.get('name'),
                      description=data.get('description'),
                      maximum_samples=global_config.maximum_samples,
                      archived=False,
                      enabled=True,
                      maximum_iteration=int(data.get('maximum_iteration')),
                      timeout=int(data.get('timeout')),
                      date=datetime.datetime.now(),
                      mutation_engine=engine,
                      fuzzer=Fuzzer.query.get(data.get('fuzzer')),
                      verifier=Verifier.query.get(data.get('verifier')),
                      seeds=seed_file,
                      fuzzing_target=new_target,
                      cmd_args=data.get('cmd_args'),
                      firmware_root=fw_file,
                      owner=current_user)
        JobsApi.add_job(new_job)
        return flask.redirect("/jobs/show")


@jobs.route('/jobs/delete/<int:job_id>', methods=['GET', 'POST'])
@login_required
def delete_job(job_id):
    if job_id is None:
        flask.abort(400, description="Invalid job ID")
    if flask.request.method == 'POST':
        if not JobsApi.delete_job(job_id):
            logging.error('User %s can not delete job with id %s' %
                          (current_user.email, str(job_id)))
            flask.flash('You are not allowed to delete this job.')
    else:
        return flask.render_template('jobs_delete.html', id=job_id)
    return flask.redirect('/jobs/show')


@jobs.route('/jobs/edit/<int:job_id>', methods=['GET', 'POST'])
@login_required
def edit_job(job_id):
    # TODO prefill form with current values
    job = Job.query.get(job_id)
    if job is None:
        flask.abort(400, description="Invalid job ID")
    if not JobsApi.is_job_admin(job.owner):
        flask.flash('You are not allowed to edit this job')
        return flask.redirect("/jobs/show")
    if flask.request.method == 'POST':
        data = flask.request.form
        engine = data.get('mutation_engine')

        Job.query.filter_by(id=job_id).update({
            'name': data.get('name'),
            'description': data.get('description'),
            'fuzzer_id': data.get('fuzzer'),
            'target_id': data.get('target'),
            'mutation_engine': engine,
            'verifier_id': data.get('verifier'),
        })
        db.session.commit()

        return flask.redirect("/jobs/show")
    else:
        fuzzers = Fuzzer.query.all()
        verifiers = Verifier.query.all()
        targets = Target.query.all()
        engines = [x['name'] for x in global_config.mutation_engines]
        return flask.render_template('jobs_edit.html',
                                     job=job,
                                     engines=engines,
                                     fuzzers=fuzzers,
                                     targets=targets,
                                     verifiers=verifiers)


def _get_summary_for_crash(crash):
    res = {}
    res['crash_signal'] = crash.crash_signal
    res['exploitability'] = crash.exploitability
    res['date'] = crash.timestamp.strftime("%Y-%m-%d %H:%M:%S")
    res['additional'] = crash.additional
    res['crash_hash'] = crash.crash_hash
    res['verfied'] = crash.verified
    res['filename'] = crash.filename
    return res


@jobs.route("/jobs/download/<int:job_id>")
@login_required
def jobs_download(job_id):
    # FIXME may crash if no crashes available
    if job_id is None:
        flask.flash("Invalid job ID")
        return flask.redirect('/jobs/show')

    job = Job.query.get(job_id)
    if not JobsApi.is_job_admin(job.owner):
        flask.flash('User is not allowed to download job.')
        return flask.redirect('/jobs/show')

    if job.crashes:
        imz = InMemoryZip()
        summary = {}
        for c in job.crashes:
            summary[str(c.id)] = _get_summary_for_crash(c)
            imz.append("%s" % str(c.id), c.test_case)
        imz.append("summary.json", json.dumps(summary, indent=4))

        filename = os.path.join('/tmp', '%s.zip' % job_id)
        if os.path.exists(filename):
            os.remove(filename)
        imz.writetofile(filename)
        return flask.send_file(filename, as_attachment=True)
    else:
        flask.flash('No crashes available to download.')
        return flask.redirect('/jobs/show')
