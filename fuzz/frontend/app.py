import os
import logging
import tracemalloc
import click
from flask import Flask, redirect, render_template, request, jsonify
from flask.logging import default_handler
from flask_migrate import Migrate
from flask_security import login_required, logout_user
from flask_behind_proxy import FlaskBehindProxy
from flask_executor import Executor


from fuzz.frontend.security import add_flask_security
from fuzz.instance.config import app_config
from fuzz.frontend.jinja2_custom import exploitable_color, map_signal_to_string, friendly_time, datetime_to_str, bin_to_hex

from fuzz.database import db
from fuzz.database.models import User, Job, Crash, Stat


logger = logging.getLogger()
logger.addHandler(default_handler)
logger.setLevel(logging.DEBUG)
executor = Executor()


def create_celery_app(app=None):
    import Celery
    CELERY_TASK_LIST = ['fuzz.frontend.tasks']
    app = app or create_app()
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_app(config_name=None):
    app = Flask(__name__, instance_relative_config=True)
    if config_name is None:
        config_name = os.getenv('APP_SETTINGS')
    app.config.from_object(app_config[config_name])

    logger.info("URI %s", app.config['SQLALCHEMY_DATABASE_URI'])

    db.init_app(app)
    migrate = Migrate(app, db)
    # Setup Flask-Security
    add_flask_security(app)
    # Add proxy support (nginx)
    FlaskBehindProxy(app)
    # Add Flask Executor
    executor.init_app(app)

    # register blueprints
    from fuzz.frontend.Users import users
    app.register_blueprint(users)
    from fuzz.frontend.Jobs import jobs
    app.register_blueprint(jobs)
    from fuzz.frontend.Crashes import crashes
    app.register_blueprint(crashes)
    from fuzz.frontend.Stats import stats
    app.register_blueprint(stats)
    from fuzz.frontend.api import jobs_api
    app.register_blueprint(jobs_api)
    from fuzz.frontend.api import crash_api
    app.register_blueprint(crash_api)
    from fuzz.frontend.api import stats_api
    app.register_blueprint(stats_api)

    # jinja custom funcs
    app.jinja_env.globals.update(exploitable_color=exploitable_color)
    app.jinja_env.globals.update(map_signal_to_string=map_signal_to_string)
    app.jinja_env.globals.update(humantime=friendly_time)
    app.jinja_env.globals.update(timediff=datetime_to_str)
    app.jinja_env.filters['time_ago'] = datetime_to_str
    app.jinja_env.filters['to_hex'] = bin_to_hex

    app.app_context().push()

    # Configure flask shell
    @app.shell_context_processor
    def make_shell_context():
        return {'db': db,
                'User': User,
                'Job': Job,
                'Crash': Crash,
                'Stat': Stat}

    # Custom datetime Jinja2 filter
    @app.template_filter('shortdatetime')
    def format_datetime(value, format="%d-%m-%Y"):
        """Format a date time to : d m YYYY"""
        if value is None:
            return ""
        return value.strftime(format)

    @app.route("/")
    @app.route("/home")
    def index():
        return render_template("home.html")

    @app.route("/about")
    def about():
        return render_template("about.html")

    @app.route("/logout")
    @login_required
    def logout():
        logout_user()
        return redirect("/home")

    @app.route('/app_metric')
    def get_metric():
        import tracemalloc
        global s1, s2
        trace = request.values.get('trace', None)
        if trace == 's2':
            s2 = tracemalloc.take_snapshot()
            for i in s2.compare_to(s1, 'lineno')[:10]:
                logger.info(i)
        elif trace == 's1':
            s1 = tracemalloc.take_snapshot()
            logger.info('trace 1 taken: Top 3')
            for i, stat in enumerate(s1.statistics('filename')[:5], 1):
                logger.info("%s %s", i, stat)
        if trace == 's3':
            s3 = tracemalloc.take_snapshot()
            print('trace 3 taken: Top 5')
            for i, stat in enumerate(s3.statistics('lineno')[:5], 1):
                logger.info("{} {}".format(i, stat))
        return jsonify(data={'success': True}), 200

    @app.cli.command('snapshot')
    @click.argument('trace')
    def shell_snapshot(trace):
        s = tracemalloc.take_snapshot()
        print('trace 1 taken: Top 10')
        for i, stat in enumerate(s.statistics('filename')[:10], 1):
            print("{} {}".format(i, stat))

    return app
