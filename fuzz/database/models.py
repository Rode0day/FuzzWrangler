from datetime import datetime
from flask_security import UserMixin, RoleMixin
from fuzz.database import db, roles_users, BaseModel
from marshmallow import Schema, fields, validates_schema, ValidationError, EXCLUDE


class Role(db.Model, RoleMixin):
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(), unique=True)
    description = db.Column(db.String())

    # __str__ is required by Flask-Admin, so we can have human-readable values
    #         for the Role when editing a User.
    def __str__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type:
    #          'Role' when saving a User
    def __hash__(self):
        return hash(self.name)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(), index=True, unique=True)
    name = db.Column(db.String())
    password = db.Column(db.String())
    active = db.Column(db.Boolean, default=True)
    registration_date = db.Column(db.DateTime())
    api_key = db.Column(db.String(), index=True, unique=True)
    fs_uniquifier = db.Column(db.String(255), unique=True, nullable=False)
    roles = db.relationship(
        'Role',
        secondary=roles_users,
        backref=db.backref('users', lazy='dynamic'))
    jobs = db.relationship('Job', backref='owner', lazy=True)

    def __repr__(self):
        return '<User {} [{}]>'.format(self.email, self.name)


class User2(BaseModel, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(), index=True, unique=True)
    name = db.Column(db.String())
    password = db.Column(db.String())
    active = db.Column(db.Boolean, default=True)
    registration_date = db.Column(db.DateTime())
    api_key = db.Column(db.String(), index=True, unique=True)
    fs_uniquifier = db.Column(db.String(255), unique=True, nullable=False)

    _default_fields = [
        "name",
        "email",
        "api_key"
    ]

    _hidden_fields = [
        "password",
    ]

    _readonly_fields = [
        "email_confirmed"
    ]


class Fuzzer(BaseModel):
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String())
    description = db.Column(db.String())
    version     = db.Column(db.String())
    url         = db.Column(db.String())
    docker      = db.Column(db.String())
    jobs        = db.relationship('Job', backref='fuzzer', lazy=True)

    def __repr__(self):
        return "<Fuzzer: {} {}>".format(self.name, self.version)


class FuzzerSchema(Schema):
    id          = fields.Int()
    name        = fields.Str(validate=lambda s: 2 < len(s) < 64, required=True)
    description = fields.Str()
    version     = fields.Str()
    url         = fields.Url()
    docker      = fields.Url()

    @validates_schema
    def name_or_id(self, data, **kwargs):
        if 'name' not in data and 'id' not in data:
            raise ValidationError('must supply either id or name')


class Verifier(BaseModel):
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String())
    description = db.Column(db.String())
    jobs        = db.relationship('Job', backref='verifier', lazy=True)

    def __repr__(self):
        return "<Verifier: {}>".format(self.name)


class VerifierSchema(Schema):
    class Meta:
        unknown = EXCLUDE
    id          = fields.Int()
    name        = fields.Str()
    description = fields.Str()

    @validates_schema
    def name_or_id(self, data, **kwargs):
        if 'name' not in data and 'id' not in data:
            raise ValidationError('must supply either id or name')


class Target(BaseModel):
    id           = db.Column(db.Integer, primary_key=True)
    name         = db.Column(db.String())
    filename     = db.Column(db.String())
    source       = db.Column(db.String())
    src_provided = db.Column(db.Boolean)
    challenge_id = db.Column(db.Integer)
    architecture = db.Column(db.String())
    archive      = db.Column(db.LargeBinary)
    known_bugs   = db.Column(db.Integer)
    bin_hash     = db.Column(db.BINARY(20), unique=True, index=True)
    binary       = db.Column(db.LargeBinary)
    jobs         = db.relationship('Job', backref='fuzzing_target', lazy=True)

    def __repr__(self):
        return "<Fuzz Target: {}".format(self.name)


class TargetSchema(Schema):
    class Meta:
        unknown = EXCLUDE
    id           = fields.Int()
    name         = fields.Str(validate=lambda s: 2 < len(s) < 255, required=True)
    source       = fields.Str()
    src_provided = fields.Bool()
    challenge_id = fields.Int()
    architecture = fields.Str()
    filename     = fields.Str()
    source       = fields.Str()
    known_bugs   = fields.Int()
    bin_hash     = fields.Str()

    @validates_schema
    def name_or_hash(self, data, **kwargs):
        if 'name' not in data and 'bin_hash' not in data:
            raise ValidationError('must supply either id or hash')


class Stat(BaseModel):
    id                = db.Column(db.Integer, primary_key=True)
    iteration         = db.Column(db.Integer)
    afl_banner        = db.Column(db.String())
    afl_version       = db.Column(db.String())
    bitmap_cvg        = db.Column(db.Float)
    command_line      = db.Column(db.String())
    cur_path          = db.Column(db.Integer)
    cycles_done       = db.Column(db.Integer)
    exec_timeout      = db.Column(db.Integer)
    execs_done        = db.Column(db.BigInteger)
    execs_per_sec     = db.Column(db.Float)
    execs_since_crash = db.Column(db.BigInteger)
    fuzzer_hostname   = db.Column(db.String())
    fuzzer_pid        = db.Column(db.Integer)
    alive             = db.Column(db.Boolean, default=True)
    last_crash        = db.Column(db.DateTime)
    last_hang         = db.Column(db.DateTime)
    last_path         = db.Column(db.DateTime)
    last_update       = db.Column(db.DateTime)
    max_depth         = db.Column(db.Integer)
    paths_favored     = db.Column(db.Integer)
    paths_found       = db.Column(db.Integer)
    paths_imported    = db.Column(db.Integer)
    paths_total       = db.Column(db.Integer)
    pending_favs      = db.Column(db.Integer)
    pending_total     = db.Column(db.Integer)
    runtime           = db.Column(db.Integer)
    stability         = db.Column(db.Float)
    start_time        = db.Column(db.DateTime)
    target_mode       = db.Column(db.String())
    unique_crashes    = db.Column(db.Integer)
    unique_hangs      = db.Column(db.Integer)
    variable_paths    = db.Column(db.Integer)
    fuzz_bitmap       = db.Column(db.LargeBinary)
    plot_data         = db.Column(db.LargeBinary)
    fuzzer_console    = db.Column(db.Text)
    job_id            = db.Column(db.Integer, db.ForeignKey('job.id', ondelete='cascade'))
    crashes           = db.relationship('Crash', backref='stat', lazy=True)

    _readonly_fields = [
        "job_id",
        "start_time"
    ]

    def __repr__(self):
        return "<Stat for job {}: crashes={} runtime={}".format(
               self.job_id,
               self.unique_crashes,
               self.runtime)


class StatSchema(Schema):
    id                = fields.Int(allow_none=True)
    job_secret        = fields.Int()
    iteration         = fields.Int()
    start_time        = fields.Raw()
    last_update       = fields.Raw()
    fuzzer_pid        = fields.Int()
    alive             = fields.Bool()
    cycles_done       = fields.Int()
    execs_done        = fields.Int()
    execs_per_sec     = fields.Float()
    paths_total       = fields.Int()
    paths_found       = fields.Int()
    paths_favored     = fields.Int()
    paths_imported    = fields.Int()
    max_depth         = fields.Int()
    cur_path          = fields.Int()
    pending_favs      = fields.Int()
    pending_total     = fields.Int()
    variable_paths    = fields.Int()
    stability         = fields.Float()
    bitmap_cvg        = fields.Float()
    unique_crashes    = fields.Int()
    unique_hangs      = fields.Int()
    last_path         = fields.Raw()
    last_crash        = fields.Raw()
    last_hang         = fields.Raw()
    execs_since_crash = fields.Int()
    exec_timeout      = fields.Int()
    afl_banner        = fields.Str()
    afl_version       = fields.Str()
    target_mode       = fields.Str()
    command_line      = fields.Str()
    runtime           = fields.Int()
    slowest_exec_ms   = fields.Int()


class Crash(BaseModel):
    __table_args__ = (
        db.UniqueConstraint('crash_hash', 'job_id', name='unique_crash'),)
    id             = db.Column(db.Integer, primary_key=True)
    crash_signal   = db.Column(db.Integer, unique=False, nullable=True)
    exploitability = db.Column(db.String())
    timestamp      = db.Column(db.DateTime, default=datetime.utcnow)
    crash_hash     = db.Column(db.BINARY, index=True)
    verified       = db.Column(db.Integer, default=0)
    bug_id         = db.Column(db.Integer, index=True)
    stack_hash_maj = db.Column(db.String())
    stack_hash_min = db.Column(db.String())
    additional     = db.Column(db.Text)
    filename       = db.Column(db.String())
    bb_count       = db.Column(db.Integer)
    bb_counts      = db.Column(db.JSON)
    bb_count_a     = db.Column(db.Integer)
    bb_count_ma    = db.Column(db.Integer)
    bb_count_mu    = db.Column(db.Integer)
    be_count_mu    = db.Column(db.Integer)
    # LargeBinary  = BYTEA for PostgreSQL
    test_case      = db.Column(db.LargeBinary)
    iteration      = db.Column(db.Integer)
    job_id         = db.Column(db.Integer, db.ForeignKey('job.id', ondelete='cascade'))
    s_id           = db.Column(db.Integer, db.ForeignKey('stat.id'))

    _readonly_fields = [
        "timestamp",
        "crash_hash",
        "test_case",
        "job_id",
        "s_id"]

    _hidden_fields = [
        "test_case"]

    def __repr__(self):
        return '<Crash {} SIG:{} V:{}'.format(
            self.job_id,
            self.crash_signal,
            self.verified)


class CrashSchema(Schema):
    id             = fields.Int()
    crash_signal   = fields.Int()
    signal         = fields.Int()
    exploitability = fields.Str()
    timestamp      = fields.DateTime()
    crash_hash     = fields.Str()
    verified       = fields.Int()
    bug_id         = fields.Int()
    stack_hash_maj = fields.Str()
    stack_hash_min = fields.Str()
    additional     = fields.Str()
    filename       = fields.Str()
    bb_count       = fields.Int()
    bb_counts      = fields.Str()
    bb_count_a     = fields.Int()
    bb_count_ma    = fields.Int()
    bb_count_mu    = fields.Int()
    be_count_mu    = fields.Int()
    iteration      = fields.Int()
    job_secret     = fields.Str()
    job_id         = fields.Int()
    s_id           = fields.Int()
    stat_id        = fields.Int()
    r0_api_token   = fields.Str()
    stack_hash     = fields.Str()


class Job(BaseModel):
    id            = db.Column(db.Integer, primary_key=True)
    job_secret    = db.Column(db.String(), unique=True)
    name          = db.Column(db.String())
    description   = db.Column(db.Text)
    date          = db.Column(db.DateTime)
    maximum_samples = db.Column(db.Integer, default=0)
    maximum_iteration = db.Column(db.Integer, default=1000000)
    duration      = db.Column(db.Integer, default=24)
    enabled       = db.Column(db.Boolean, default=True)
    archived      = db.Column(db.Boolean, default=False)
    mutation_engine = db.Column(db.String())
    cmd_args      = db.Column(db.String())
    total_execs   = db.Column(db.BigInteger, default=0)
    total_runtime = db.Column(db.Integer)
    cov_line      = db.Column(db.Float)
    cov_func      = db.Column(db.Float)
    cov_branch    = db.Column(db.Float)
    cov_bb        = db.Column(db.Integer)
    cov_bbe       = db.Column(db.Integer)
    cov_json      = db.Column(db.Text)
    mon_blocks    = db.Column(db.Integer)
    mon_edges     = db.Column(db.Integer)
    mon_bugs      = db.Column(db.Integer)
    alive         = db.Column(db.Integer)
    crashes       = db.relationship('Crash', backref='job',
                                    cascade="all, delete-orphan",
                                    lazy=True, passive_deletes=True)
    stats         = db.relationship('Stat', backref='job',
                                    cascade="all, delete-orphan",
                                    lazy=True, passive_deletes=True)
    owner_id      = db.Column(db.Integer, db.ForeignKey('user.id'))
    target_id     = db.Column(db.Integer, db.ForeignKey('target.id'))
    fuzzer_id     = db.Column(db.Integer, db.ForeignKey('fuzzer.id'))
    verifier_id   = db.Column(db.Integer, db.ForeignKey('verifier.id'))
    seeds         = db.Column(db.LargeBinary)
    firmware_root = db.Column(db.LargeBinary)
    plot_data     = db.Column(db.LargeBinary)

    _hidden_fields = [
        "seeds",
        "firmware_root",
        "plot_data"
    ]

    def __repr__(self):
        return "<Job {}".format(self.name)


class JobSchema(Schema):
    id                = fields.Int()
    job_secret        = fields.Str(required=True)
    name              = fields.Str(validate=lambda s: 2 < len(s) < 255, required=True)
    description       = fields.Str(required=True)
    date              = fields.DateTime()
    maximum_samples   = fields.Int()
    maximum_iteration = fields.Int()
    duration          = fields.Int()
    enabled           = fields.Bool()
    archived          = fields.Bool()
    mutation_engine   = fields.Str()
    cmdline           = fields.Str()
    cmd_args          = fields.Str()
    total_execs       = fields.Int()
    total_runtime     = fields.Int()
    cov_line          = fields.Float(allow_none=True)
    cov_func          = fields.Float(allow_none=True)
    cov_branch        = fields.Float(allow_none=True)
    cov_bb            = fields.Int(allow_none=True)
    cov_bbe           = fields.Int(allow_none=True)
    cov_json          = fields.Str()
    mon_blocks        = fields.Int(allow_none=True)
    mon_edges         = fields.Int(allow_none=True)
    mon_bugs          = fields.Int(allow_none=True)
    alive             = fields.Int(allow_none=True)
    target_info       = fields.Nested(TargetSchema)
    fuzzer_info       = fields.Nested(FuzzerSchema)
    verifier          = fields.Nested(VerifierSchema)
